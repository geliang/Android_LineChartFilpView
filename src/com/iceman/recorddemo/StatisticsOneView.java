package com.iceman.recorddemo;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.View;

/**
 * 统计图实例（注：其中有部分值没有实际意义，只是为了达到更好的展示效果，有需要有可以做适当修改）
 * 
 * @author Harlan Song
 * @weibo: weibo.com/markdev 2012-8-29
 */
public class StatisticsOneView extends View {
	private int xPoint = 0;// 原点X坐标
	private int yPoint = 0;// 原点Y坐标
	private int xLengh = 240;// X轴长度
	private int yLengh = 320;// Y轴长度
	private int xScale = 24;// X轴一个刻度长度
	private int yScale = 25;// Y轴一个刻度长度
	private int widthBorder = 40;// 内边缘宽度，为了统计图不靠在屏幕的边缘上，向边缘缩进距离。最好大于30。
	private String[] xLableArray;// X轴标签
	private float[] yLableArray;// Y轴标签
	private int[] dataArray;// 画折线的数值
	private float x_current;
	private float y_current;
	private int index_detail_data = 0;
	private Paint paint;
	private float x_lastDown;
	private float y_lastDown;

	public StatisticsOneView(Context context) {
		super(context);

	}

	/**
	 * 实例化值
	 * 
	 * @param screenWidth
	 *            手机屏幕宽度
	 * @param ScreenHeight
	 *            手机屏幕高度
	 * @param xLable
	 *            X轴标签
	 * @param yLable
	 *            Y轴标签
	 * @param dataArray
	 *            统计数据
	 */
	public void initValue(int screenWidth, int ScreenHeight, int[] dataArray) {
		// 38 是手机状态栏的大概高度，这是i9100上测试的，如果应用有标题栏需要在这个基础上加，差不多两倍吧。
		xPoint += widthBorder;
		yPoint = ScreenHeight - widthBorder - 38;
		xLengh = screenWidth - widthBorder * 2;
		yLengh = ScreenHeight - widthBorder * 2 - 38;
		xLableArray = new String[dataArray.length];
		int dataArrayLength = dataArray.length;
		xScale = ((xLengh / dataArrayLength) > xScale) ? (xLengh / dataArrayLength)
				: xScale;
		int maxY = 0;
		for (int i = 0; i < dataArrayLength; i++) {
			xLableArray[i] = "" + i;
			maxY = dataArray[i] > maxY ? dataArray[i] : maxY;
		}
		int ySize = (yLengh % yScale == 0) ? (yLengh / yScale)
				: ((yLengh / yScale) + 1);
		yLableArray = new float[ySize];
		// float offset = Math.round((float)maxY/ySize);//兼容小数点.
		float offset = (float) maxY / ySize;
		for (int i = 0; i < ySize; i++) {
			yLableArray[i] = (i * offset);
		}
		this.dataArray = dataArray;
		mDetailPopuWindow = new DetailPopuWindow();
		// 设置画笔
		paint = new Paint();
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		paint.setStrokeWidth(1f);
		paint.setStyle(Paint.Style.STROKE);// 设置画笔样式
		paint.setAntiAlias(true);// 去锯齿
		paint.setColor(Color.BLACK);// 设置颜色
		paint.setTextSize(12);// 设置字体
		// 画X轴轴线
		canvas.drawLine(xPoint, yPoint, xPoint + xLengh + xScale, yPoint, paint);
		// 画X轴箭头
		canvas.drawLine(xPoint + xScale + xLengh - 6, yPoint - 3, xPoint
				+ xLengh + xScale, yPoint, paint);
		canvas.drawLine(xPoint + xScale + xLengh - 6, yPoint + 3, xPoint
				+ xLengh + xScale, yPoint, paint);// TODO 补充所有的线
		// 画Y轴轴线
		canvas.drawLine(xPoint, yPoint, xPoint, yPoint - yLengh - yScale, paint);
		// 画Y轴箭头
		canvas.drawLine(xPoint + 3, yPoint - yLengh + 6 - yScale, xPoint,
				yPoint - yLengh - yScale, paint);
		canvas.drawLine(xPoint - 3, yPoint - yLengh + 6 - yScale, xPoint,
				yPoint - yLengh - yScale, paint);

		for (int i = 0; xLableArray != null && i < xLableArray.length; i++) {
			// 画X轴刻度
			canvas.drawLine(xPoint + xScale * i, yPoint - 3, xPoint + xScale
					* i, yPoint, paint);
			// 画X轴刻度标签
			canvas.drawText(xLableArray[i], xPoint + xScale * i - 16,
					yPoint + 15, paint);
			paint.setColor(Color.argb(255, 18, 158, 204));// 设置颜色
			paint.setStrokeWidth(2f);
			// 画折线
			if (dataArray != null && dataArray.length > 0
					&& i < dataArray.length) {
				int ydata = getYDataPoint(dataArray[i]);
				if (ydata != 0) {
					canvas.drawCircle(xPoint + xScale * i, ydata, 2, paint);
					if (dataArray.length > i + 1) {
						canvas.drawLine(xPoint + xScale * i,
								getYDataPoint(dataArray[i]), xPoint + xScale
										* (i + 1),
								getYDataPoint(dataArray[i + 1]), paint);
					}
				}

			}
			paint.setStrokeWidth(1f);
		}

		for (int i = 0; yLableArray != null && i < yLableArray.length; i++) {
			// 画Y轴刻度
			canvas.drawLine(xPoint, yPoint - yScale * i, xPoint + 3, yPoint
					- yScale * i, paint);
			// 画Y轴刻度标签

			canvas.drawText(
					(yLableArray[i] > 10 && String.valueOf(yLableArray[i])
							.length() > 2) ? String.format("%.0f",
							yLableArray[i]) : String.format("%.2f",
							yLableArray[i]), xPoint - 30, yPoint - yScale * i
							+ 3, paint);
		}
		drawDetailPopuWindow(canvas);
	}

	private void drawDetailPopuWindow(Canvas canvas) {
		if (mDetailPopuWindow.isShow()) {
			int y = mDetailPopuWindow.y;
			int x = mDetailPopuWindow.x;
			paint.setTextSize(32);
			
			canvas.drawText(mDetailPopuWindow.getTitle() + "",
					mDetailPopuWindow.x, y, paint);
			
			double random =Math.random();
			paint.setAlpha((int) (255*(1-random)));
			paint.setStrokeWidth((float) Math.random()*10);
			paint.setStyle(Paint.Style.FILL_AND_STROKE);
			canvas.drawCircle(
					mDetailPopuWindow.x, y,(float) ((int)20*random), paint);
			paint.setAlpha(255);
			
			paint.setStrokeWidth(1f);
			
			paint.setColor(Color.argb(255, 161, 199, 57));// 设置颜色
			paint.setTextSize(48);
			if (x + 20 > yLengh) {
				x -= 20;
				x -= paint.measureText(mDetailPopuWindow.getContent());
				y -= 40;
			}
			canvas.drawText(mDetailPopuWindow.getContent() + "", x + 20,
					y + 18, paint);
			paint.setColor(Color.argb(255, 18, 158, 204));// 设置颜色
			
			paint.setStrokeWidth(2f);
			// 画指示线
			canvas.drawLine(mDetailPopuWindow.x, mDetailPopuWindow.y, xPoint
					+ xScale * (mDetailPopuWindow.index),
					getYDataPoint(dataArray[mDetailPopuWindow.index]), paint);
			paint.setStrokeWidth(1f);
		}
		System.out.println(getTop() + ">>" + getScrollY());
	}

	/**
	 * 得到每一等分的长度
	 * 
	 * @param num
	 *            所要分成的等份
	 * @param length
	 *            要分割的总长度
	 * @return
	 */
	private int getScale(int num, int length) {
		if (num > 0 && length > 0) {
			length -= 10;// 为了美观，缩进
			length = length - (length % num);
			return length / num;
		} else {
			return 0;
		}

	}

	/**
	 * 得到点的Y坐标
	 * 
	 * @param data
	 * @return
	 */
	private int getYDataPoint(int data) {
		int y = (int) (yPoint - (((float) data / (float) yLableArray[yLableArray.length - 1]) * yLengh));
		// double yScale_db = (double) yScale;
		// double pxSize = yScale_db / yLableArray[1];
		// int pxSize_int = (int) pxSize;
		// int last = 0;
		// if ((last = data % yLableArray[1]) == 0) {
		// y = yPoint - (data / yLableArray[1] * yScale);
		// } else {
		// y = yPoint - (data / yLableArray[1] * yScale) - pxSize_int * last;
		// }

		return y;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		try {
			x_current = event.getX();
			y_current = event.getY();
			switch (event.getAction() & MotionEvent.ACTION_MASK) {
			case MotionEvent.ACTION_DOWN:
				mDetailPopuWindow.setShow(true);
				x_lastDown = event.getX();
				y_lastDown = event.getY();
				break;
			case MotionEvent.ACTION_POINTER_DOWN:
				break;
			case MotionEvent.ACTION_MOVE:
				mDetailPopuWindow.x = (int) x_current;
				mDetailPopuWindow.y = (int) y_current - yLengh/2;
				mDetailPopuWindow.setTitle(""
						+ xLableArray[getIndexFromDataArray(x_current,
								y_current)]);
				mDetailPopuWindow
						.setContent(""
								+ dataArray[getIndexFromDataArray(x_current,
										y_current)]);
				mDetailPopuWindow.index = getIndexFromDataArray(x_current,
						y_current);
				invalidate();
				break;
			case MotionEvent.ACTION_UP:
				// if (x_current>xLengh) {
				// scrollBy(20, 0);
				// }else if(x_current<xPoint){
				// scrollBy(-20, 0);
				// } // 支持滑动
			case MotionEvent.ACTION_OUTSIDE:
			default:
				mDetailPopuWindow.setShow(false);
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return true;
		}

	}

	private int getIndexFromDataArray(float x_current2, float y_current2) {
		if (dataArray == null || (x_current2 - xPoint) <= 0) {
			return 0;
		}
		int index = (int) ((x_current2 - xPoint) / xScale);
		index = (index > dataArray.length - 1) ? dataArray.length - 1 : index;
		return index;
	}

	DetailPopuWindow mDetailPopuWindow;

	class DetailPopuWindow {
		public int index;
		String title;
		String content;

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public String getContent() {
			return content;
		}

		public void setContent(String content) {
			this.content = content;
		}

		public boolean isShow() {
			return show;
		}

		public void setShow(boolean show) {
			this.show = show;
		}

		int x;
		int y;
		boolean show;
	}

}