package com.iceman.recorddemo;


import android.app.Activity;
import android.os.Bundle;
import android.widget.LinearLayout;

public class StatisticsOne extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		LinearLayout linearLayout = (LinearLayout) findViewById(R.id.body);
		StatisticsOneView statisticsOneView = new StatisticsOneView(this);
		String[] xScaleArray = { "星期一", "星期二", "星期三", "星期四", "星期五", "星期六",
				"星期日" };
//		String[] xScaleArray  = new String[20];
		for (int i = 0; i < xScaleArray.length; i++) {
			xScaleArray[i]=(i+1)+"";
		}
//		int[] yXcaleArray = { 0, 10, 20, 30, 40, 50, 60 ,70,80,90,100,110,120,130,140};
//		int[] dataArray = { 0, 0, 1, 0,0,0,0,0,0,0,0,0,0};
		int[] dataArray = { 300, 50, 60, 50, 40, 32 ,60,20,1,100,1000,1,1,2,2,2,2,2,2,2,2,2,2,200,2,2,2,2,2,2,200,0};
//		statisticsOneView.initValue(480, 800, xScaleArray, yXcaleArray,
//				dataArray);
		statisticsOneView.initValue(800, 460,
				dataArray);
		linearLayout.addView(statisticsOneView);

	}

}