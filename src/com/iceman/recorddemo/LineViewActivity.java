package com.iceman.recorddemo;

import android.app.Activity;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aphidmobile.flip.FlipViewController;
import com.aphidmobile.flip.FlipViewController.ViewFlipListener;
import com.iceman.recorddemo.LineView.DetailPopuWindow;
import com.iceman.recorddemo.LineView.onShowDetailInfo;

/**
 * demo for line view <br>
 * 第三页滑动的过程程序会崩毁,提醒使用者该组件对多个长度不一线条不支持.
 * 
 * @author Administrator
 * 
 */
public class LineViewActivity extends Activity {
	int[] a = { 130, 140, 200, 412, 54, 62, 9, 234, 76, 21, 546, 23, 67, 12,
			66, 1, 7 };
	int[] b = { 0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140,
			200, 412, 54, 62, 9, 234, 76, 21, 500, 23, 67, 12, 66, 1, 7 };
	int[] c = { 234, 76, 21, 800, 23, 67, 12, 66, 1, 7 };
	int[] d = { 12, 66, 77, 44, 22, 99, 22, 1, 122, 333 };
	int[] e = new int[30];
	int[][] dataArrays = { b, a, c, d, e };
	int[][] dataArrays2 = { a, c, d, e, b, };
	int[][] dataArrays3 = { b };
	int[][][] dataArrays_example = { dataArrays, dataArrays3, dataArrays2 };
	private TextView context;
	private TextView tv_title;
	private View rl_content;
	private FlipViewController flipView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		// setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

		setContentView(R.layout.activity_main);
		LinearLayout linearLayout = (LinearLayout) findViewById(R.id.body);
		String[] xScaleArray = { "星期一", "星期二", "星期三", "星期四", "星期五", "星期六",
				"星期日" };
		// String[] xScaleArray = new String[20];
		for (int i = 0; i < xScaleArray.length; i++) {
			xScaleArray[i] = (i + 1) + "";
		}
		// int[] yXcaleArray = { 0, 10, 20, 30, 40, 50, 60
		// ,70,80,90,100,110,120,130,140};
		// int[] dataArray = { 0, 0, 1, 0,0,0,0,0,0,0,0,0,0};

		// statisticsOneView.initValue(480, 800, xScaleArray, yXcaleArray,
		// dataArray);

		// statisticsOneView.initValue(460, 760, dataArray);

		tv_title = (TextView) findViewById(R.id.textView1);
		context = (TextView) findViewById(R.id.textView2);
		rl_content = findViewById(R.id.rl_context);

		OnClickListener mOnClickListener = new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				rl_content.setVisibility(View.GONE);
				// statisticsOneView.invalidate();
			}
		};
		final onShowDetailInfo monShowDetailInfo = new onShowDetailInfo() {

			@Override
			public void showDetailInfo(DetailPopuWindow... detailPopuWindow) {

				for (DetailPopuWindow d : detailPopuWindow) {
					System.out.println(d.title + "->" + d.content + "->"
							+ d.index);
				}
				if (rl_content.getVisibility() == View.GONE) {
					rl_content.setVisibility(View.VISIBLE);
				}
				// tv_title.setText("" + dataArray[indexInDataArray]);
			}
		};
		context.setOnClickListener(mOnClickListener);

		tv_title = (TextView) findViewById(R.id.textView1);
		context = (TextView) findViewById(R.id.textView2);
		rl_content = findViewById(R.id.rl_context);
		context.setOnClickListener(mOnClickListener);
		// linearLayout.addView(statisticsOneView);

		flipView = (FlipViewController) findViewById(R.id.flipView);
		flipView.setTouchSlop(400);
		final SoundPool sound = new SoundPool(10, AudioManager.STREAM_MUSIC, 5);
		final int sound_PageFlip;
		sound_PageFlip = sound.load(this, R.raw.page_flip_short, 1);
		flipView.setOnViewFlipListener(new ViewFlipListener() {
			@Override
			public void onViewFlipped(View view, int position) {
				sound.play(sound_PageFlip, 1, 1, 0, 0, 1);
				// System.out.println("position:" + position);
				// int length = (int) (Math.random() * 10) + 1;
				// int[] temp = new int[length];
				// switch (position) {
				// case 0:
				//
				// for (int i = 0; i < length; i++) {
				// temp[i] = (int) (Math.random() * 10);
				// }
				//
				// dataArrays[1] = dataArrays[0];
				// dataArrays[2] = dataArrays[1];
				// dataArrays[0] = temp;
				// flipView.setSelection(1);
				// break;
				// case 2:
				// for (int i = 0; i < length; i++) {
				// temp[i] = (int) (Math.random() * 10);
				// }
				//
				// dataArrays[0] = dataArrays[1];
				// dataArrays[1] = dataArrays[2];
				// dataArrays[2] = temp;
				// flipView.setSelection(1);
				// break;
				// }
			}

		});
		flipView.setAdapter(new BaseAdapter() {
			@Override
			public int getCount() {
				return 3;
			}

			@Override
			public Object getItem(int position) {
				return position;
			}

			@Override
			public long getItemId(int position) {
				return position;
			}

			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				System.out.println(position);
				if (convertView == null) {
					convertView = new LineView(getBaseContext());
				}
				((LineView) convertView).initValue(460, 760,
						dataArrays_example[position]);
				if (position % 2 == 0) {
					((LineView) convertView)
							.setOnShowDetailInfo(monShowDetailInfo);// 自定义的统计图上的点被选中效果
				} else {
					((LineView) convertView).setOnShowDetailInfo(null);// 默然点被选中的效果.
				}
				return convertView;
			}
		});
		// flipView.setSelection(dataArrays.length / 2);
	}

	@Override
	protected void onResume() {
		super.onResume();
		flipView.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
		flipView.onPause();
	}

}